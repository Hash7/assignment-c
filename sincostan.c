
#include <stdio.h>
#include <math.h>
 
int main()
 
{
       float i = 0.314;
       float j = 0.25;
       float k = 6.25;
       float sin_value = sin(i);
       float cos_value = cos(i);
       float tan_value = tan(i);
 
       printf("The value of sin(%f) : %f \n", i, sin_value);
       printf("The value of cos(%f) : %f \n", i, cos_value);
       printf("The value of tan(%f) : %f \n", i, tan_value);
       return 0;
}