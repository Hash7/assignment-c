
#include<stdio.h>

int main() {

	int num;

	printf("Enter the Number whose Multiplication Table is to be Displayed :\n");
	scanf("%d", &num);

	printf("The Multiplication Table of %d is :\n", num);

	for ( int i=0; i<10 ; i++ ) {
		printf( "%d x %d = %d\n", num, i+1, (i+1)*num );
	}

	return 0;

}





	
